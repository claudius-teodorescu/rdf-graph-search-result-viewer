import { LitElement, html } from "https://cdn.jsdelivr.net/npm/lit/+esm";
import { map } from "https://cdn.jsdelivr.net/npm/lit/directives/map.js/+esm";
import "https://cdn.jsdelivr.net/npm/@lion/pagination@0.9.1/lion-pagination.js/+esm";
import { styles } from "./styles.js";

export default class IndexHeadingsViewer extends LitElement {
    static properties = {
        _visibleHeadings: {
            attribute: false
        },
        queryString: {
            attribute: false
        },
        itemsPerPage: {
            attribute: false
        },
        currentPage: {
            attribute: false
        },
        pageCount: {
            attribute: false
        },
    };

    static styles = styles;

    constructor() {
        super();

        this._init();
    }

    render() {
        return html`
            <sl-details disabled>
                <div slot="summary">
                    <output>
                        ${this._displaySummary()}
                    </output>
                </div>
                <div>
                    <lion-pagination count="${this.pageCount}" current="${this.currentPage}"></lion-pagination>
                    <div id="content" class="facet-all">
                        ${map(this._visibleHeadings, item => this._displayHeading(item))}
                    </div>
                </div>
            </sl-details>
        `;
    }

    createRenderRoot() {
        const root = super.createRenderRoot();

        root.addEventListener("click", event => {
            const target = event.target;

            if (target.matches("div[slot = 'summary'] a")) {
                let facet_value_index = target.dataset.facetValueIndex;
                this.content.classList.remove(...this.content.classList);

                if (facet_value_index === "all") {
                    this.content.classList.add("facet-all");
                } else {
                    this.content.classList.add("facet-" + facet_value_index);
                }

                return false;
            }

            if (target.matches("div#content div")) {
                this.content.querySelectorAll("div").forEach(item => item.classList.remove("selected"));
                target.classList.add("selected");

                root.dispatchEvent(new CustomEvent("kub-index-headings-viewer:selected", {
                    "detail": [target.dataset.value, target.dataset.id],
                    "bubbles": true,
                    "composed": true,
                }));
            }
        });

        root.addEventListener("sl-show", () => {
            root.dispatchEvent(new CustomEvent("kub-index-headings-viewer:show", {
                "bubbles": true,
                "composed": true,
            }));
        });

        root.addEventListener("sl-hide", () => {
            console.log(this._reset);
            if (this._reset) {
                this._reset = false;
            } else {
                root.dispatchEvent(new CustomEvent("kub-index-headings-viewer:hide", {
                    "bubbles": true,
                    "composed": true,
                }));
            }
        });

        return root;
    }

    firstUpdated() {
        this.details = this.renderRoot.querySelector("sl-details");
        this.summary = this.renderRoot.querySelector("div[slot = 'summary']");
        this.pagination = this.renderRoot.querySelector("lion-pagination");
        this.content = this.renderRoot.querySelector("div#content");
        this.facet_subvalues_container = this.renderRoot.querySelector("#facet-subvalues");

        this.pagination.addEventListener("current-changed", event => {
            this._displayPage(event.target.current);
        });
    }

    _init() {
        this._headings = [];
        this._visibleHeadings = [];
        this.queryString = "";
        this.itemsPerPage = 30;
        this.currentPage = 1;
        this.pageCount = 1;
        this._reset = false;

        this.facet_subvalues = {};
        this.index = [];
    }

    set headings(value) {
        this._headings = value;
        this.pageCount = Math.ceil(value.length / this.itemsPerPage);

        this._displayPage(1);
    }

    _displaySummary() {
        if (this._headings.length === 0) {
            return html``;
        } else {
            return html`
                <span>${this.queryString},</span>
                <span>${this._headings.length.toLocaleString("ro")}</span>
                <span>articol(e)</span>
            `;
        }
    }

    _displayPage(currentPage) {
        let startIndex = (currentPage - 1) * this.itemsPerPage;
        let endIndex = currentPage * this.itemsPerPage;
        this._visibleHeadings = this._headings.slice(startIndex, endIndex);
    }

    _displayHeading(headingData) {
        let [heading, heading_id] = headingData;
        heading = heading
            .replace("||", " ")
            .replace(/\|([0-9])\|/g, "<sup>$1</sup> ");

        return html`<div data-id="${heading_id}" data-value="${heading}">${heading}</div>`;
    }

    summaryTemplate(data) {
        return `<output id="facet-label">${data.facet_label}</output>, dintre care: <div id="facet-values">${data.values}</div>`;
    }

    facet_value_link_template(data) {
        return `<a class="facet-value-link" href="#" data-facet-value-index="${data.facet_value_index}">${data.label}</a>`;
    }

    show() {
        this.details.disabled = false;
        this.details.show();
    }

    hide() {
        this.details.hide();
    }

    reset() {
        this._init();        
        this._reset = true;
        this.details.disabled = true;
        this.details.hide();
    }
}

window.customElements.define("kub-index-headings-viewer", IndexHeadingsViewer);

/*updateSummaryForIndexSubentries() {
        let inverted_index = this.index.inverted_index;
        let inverted_index_length = inverted_index.length;
        let facet_value_relative_indexes = this.index.values;

        let searchResultItems = this.content.querySelectorAll("div");

        let summary_as_html = "";
        let css_selectors = [];

        let values = Object.keys(facet_value_relative_indexes)
            .sort(new Intl.Collator("ro").compare);
        let facet_label = `${inverted_index_length.toLocaleString("ro")} articol(e)`;

        if (values.length > 1) {
            let values_to_html = values
                .map((value, valueIndex) => {
                    let relative_index = facet_value_relative_indexes[value];
                    let relative_index_length = relative_index.length;

                    searchResultItems.forEach((item, itemIndex) => {
                        if (relative_index.includes(itemIndex)) {
                            item.classList.add("facet-" + valueIndex);
                        }
                    });

                    css_selectors.push(`div#content.facet-${valueIndex} > div.facet-${valueIndex}`);

                    return this.facet_value_link_template({
                        "label": `${relative_index_length} ${value}`,
                        "facet_value_index": valueIndex
                    });
                }).join("");

            let css_instruction = `<style>${css_selectors.join(", ")} {display: inline-block;}</style>`;

            summary_as_html = css_instruction +
                this.summaryTemplate({
                    "facet_label": this.facet_value_link_template({
                        "label": facet_label,
                        "facet_value_index": "all"
                    }),
                    "values": values_to_html,
                })
                ;
        } else {
            summary_as_html = `<output>${facet_label}</output>`;
        }

        this.summary.innerHTML = summary_as_html;
    }*/