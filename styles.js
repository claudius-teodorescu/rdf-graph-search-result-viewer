import { css, unsafeCSS } from "https://cdn.jsdelivr.net/npm/lit/+esm";

export const styles =
    css`
        :host {
            width: var(--kub-width, 40vw);
        }            
        div#content {
            display: flex;
            flex-wrap: wrap;
            justify-content: space-between;
            align-content: start;
            padding: 3px 0;
            overflow-y: scroll;
            overflow-x: hidden;
            height: var(--kub-height, 40vh);
            width: 100%;
        }
        div#content > div {
            width: 180px;
            padding: 3px;
            color: black;
        }
        div#content div:hover {
            background-color: #e0dddc;
        }
        div#content div.selected {
            background-color: #e0dddc;
        }        
        div#facet-values {
            display: inline;
        }
        div#facet-values > a.facet-value-link {
            margin-right: 10px;
        }
        div#content.facet-all > div {
            display: inline-block;
            text-align: initial;
        }
        /* small screen sizes */
        @media only screen and (max-width: 664px) {
            :host {
                width: 90vw;
            }
            div#content > div {
                width: 120px;
            }                    
        }
        /* medium screen sizes */
        @media only screen and (max-width: 835px) {
            :host {
                width: 90vw;
            }  
            div#content > div {
                width: 120px;
            }                    
        }         
`;